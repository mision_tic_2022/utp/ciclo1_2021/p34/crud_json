
#Importamos el controlador
import controlador

"""
UI (User Interface)
1 -> Crear tarea
    >>> Descripción:
    >>> Estado:
    >>> Tiempo:
2 -> Consultar todas las tareas
3 -> Actualizar tarea
    >>> Por favor ingrese el id de la tarea: 
        >>> Descripción:
        >>> Estado:
        >>> Tiempo:
4 -> Eliminar tarea
    >>> Por favor ingrese el id de la tarea a eliminar: 
"""
"""
descripcion: -----
estado: ---
tiempo: ---
"""

def mostrar_tareas(tareas: dict):
    for clave, tarea in tareas.items():
        print("----------------------")
        print("Descripción: ", tarea["descripcion"])
        print("Estado: ", tarea["estado"])
        print("Tiempo: ", tarea["tiempo"])
        print("----------------------")

#Menú de opciones
def menu():
    opc = -1
    tareas = controlador.read()
    claves = list(map(int, tareas.keys()))
    increment = max(claves) + 1
    
    while opc != 0:
        print("***********************************")
        print("1 -> Crear tarea")
        print("2 -> Consultar todas las tareas")
        print("3 -> Actualizar tarea")
        print("4 -> Eliminar tarea")
        print("5 -> Guardar todos los cambios")
        print("0 -> Salir")
        print("***********************************")
        #Solicita la opción al usuario
        opc = int( input("Por favor ingrese una opción: ") )
        #Opciones
        if opc == 1:
            nueva_tarea = {
                "descripcion": input("Por favor ingrese la descripción de la tarea: "),
                "estado": input("Estado de la tarea: "),
                "tiempo": int( input("Tiempo de la tarea: ") )
            }
            controlador.create(tareas, "0"+str(increment), nueva_tarea)
            increment += 1
        elif opc == 2:
            mostrar_tareas(tareas)
        elif opc == 3:
            clave = input("Por favor ingrese el id de la tarea a actualizar: ")
            if clave in tareas.keys():
                tarea_actualizada = {
                "descripcion": input("Por favor ingrese la nueva descripción de la tarea: "),
                "estado": input("Estado de la tarea: "),
                "tiempo": int( input("Tiempo de la tarea: ") )
                }
                controlador.update(tareas, clave, tarea_actualizada)
            else:
                print("La clave ingresada no existe")
        elif opc == 4:
            clave = input("Por favor ingrese el id de la tarea a eliminar: ")
            if clave in tareas.keys():
                controlador.delete(tareas, clave)
            else:
                print("La clave ingresada no existe")
        elif opc == 5:
            controlador.save(tareas)
menu()