"""
tareas = {
    "01":{
        "descripcion": "Ir a mercar",
        "estado": "pendiente",
        "tiempo": 60
    },
    ...
}
LINK REPOSITORIO:
https://gitlab.com/mision_tic_2022/utp/ciclo1_2021/p34/crud_json
"""
#Importamos biblioteca manejadora de formato json
import json

#Función para crear una tarea
def create(tareas:dict, clave: str, valor: dict)->dict:
    #Valida si la clave existe en el diccionario
    if not clave in tareas.keys():
        #Añadir una nueva clave-valor al diccionario
        tareas[clave] = valor
    return tareas

#Función para obtener(leer) todas las tareas
def read()->dict:
    #Inicializa variable vacía
    datos = None
    #try-except para el manejo de excepciones (errores en ejecución)
    try:
        #Referencia el fichero JSON en la variable 'archivo'
        with open("modelo.json") as archivo:
            #Carga los datos del JSON y los almacena como un diccionario 
            #en la variable 'datos'
            datos = json.load(archivo)
    except:
        datos = {}
        print("Error al cargar el fichero")
    
    return datos

#Función para actualizar una tarea
def update(tareas: dict, clave: str, nuevo_valor: dict)->dict:
    #Valida si la clave existe en el diccionario
    if clave in tareas.keys():
        tareas[clave] = nuevo_valor
    else:
        print("La clave indicada no existe")
    return tareas

#Función para eliminar una tarea
def delete(tareas: dict, clave: str)->dict:
    #Valida si la clave existe en el diccionario
    if clave in tareas.keys():
        #Elimino una tarea por medio de su clave
        tareas.pop(clave)
    return tareas

def save(tareas):
    with open("modelo.json", "w") as archivo:
        json.dump(tareas, archivo)
